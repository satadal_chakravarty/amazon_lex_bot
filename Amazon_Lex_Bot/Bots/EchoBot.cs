﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Lex;
using Amazon.Lex.Model;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using RestSharp;

namespace Microsoft.BotBuilderSamples.Bots
{
   
    public class EchoBot : ActivityHandler
    {
        private const string q_a = "QUESTION_ANSWER";
        private const string lexTextResponse = "";
        private string replyText = "";
        private object awsLexClient;
        
        
        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            Dictionary<string, string> lexSessionAttributes = new Dictionary<string, string>();
            lexSessionAttributes.Add("Teams","Yes");

            PostTextRequest lexTextRequest = new PostTextRequest()
            {
                BotAlias = "test",
                BotName = "HRbotSimple",
                InputText = turnContext.Activity.Text.ToString(),
                UserId = "Rajesh",
                SessionAttributes = lexSessionAttributes
            };
            
            var region = RegionEndpoint.GetBySystemName("us-east-1");
            var amazonLexClient = new AmazonLexClient("AKIAU3JMQR2KK5IBZC5V", "FAYNbs9NGgpumshEIFGmxx+qQMqA6C3//6bOy+5z", region: region);
            PostTextResponse amazonPostResponse = new Amazon.Lex.Model.PostTextResponse();


            var typingActivity = new Activity[] {
                                                new Activity { Type = ActivityTypes.Typing },
                                                //MessageFactory.Text("The Bot is searching for your query"),
                                            };

            await turnContext.SendActivitiesAsync(typingActivity, cancellationToken);

            try
            {
                amazonPostResponse = amazonLexClient.PostTextAsync(lexTextRequest).Result;
            }
            catch (Exception ex)
            {
                throw new BadRequestException(ex);
            }
            
            IMessageActivity message = Activity.CreateMessageActivity();
            message.Text = amazonPostResponse.Message.ToString();
            message.TextFormat = "markdown";
            

            //await turnContext.SendActivityAsync(MessageFactory.Text(amazonPostResponse.Message.ToString()), cancellationToken);

            await turnContext.SendActivityAsync(message, cancellationToken);
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            var welcomeText = "Hello and welcome!";
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text(welcomeText, welcomeText), cancellationToken);
                }
            }
        }
    }

       
}
